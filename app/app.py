from flask import Flask
app = Flask(__name__)

DEFAULT_STRING = "Hello Web App running on Linux!"

@app.route("/")
def hello():
    return DEFAULT_STRING

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
