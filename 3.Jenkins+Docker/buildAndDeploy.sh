#!/bin/bash -x

JENKINS_CONTAINER_NAME=jenkins-docker_demo
JENKINS_IMAGE=jenkins-docker_demo
JENKINS_IMAGE_TAG=latest

JenkinsContainerId=`docker ps -qa --filter "name=$JENKINS_CONTAINER_NAME"`

if [ -n "$JenkinsContainerId" ]
then
	echo "Stopping and removing existing jenkins container"
	docker stop $JENKINS_CONTAINER_NAME
	docker rm $JENKINS_CONTAINER_NAME
fi

docker build -t $JENKINS_IMAGE .
docker container run --name $JENKINS_CONTAINER_NAME -d \
	-p 8080:8080 \
	-p 50000:50000 \
	-v /var/run/docker.sock:/var/run/docker.sock \
	$JENKINS_IMAGE:$JENKINS_IMAGE_TAG
