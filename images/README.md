# GitFlow 

These jobs will try to build the image when a commit is pushed to the referenced branch on the job, if it is successful 
will be pushed to a Docker Registry, in order to not build the image again if wanna be deploy, the last step is run the 
container and run some test.
The container will be up if wanna be manually tested

## Develop branch
![Develop branch](jenkins_gitflow_develop_branch.png "Develop branch")

## Feature branches
![Feature branches](jenkins_gitflow_feature_branches.png "Feature branches")

## Hot-Fixes branches
![Hot-Fixes branches](jenkins_gitflow_hot-fixes_branches.png "Hot-Fixes branches")

## Master branch
![Master branch](jenkins_gitflow_master_branch.png "Master branch")

## Release branches
![Release branches](jenkins_gitflow_release_branches.png "Release branches")


# Merge to

These jobs are configured to select one branch (manually) and be merged to the target branch. 

## Hot-Fix

### Hot-Fix to develop
![Hot-Fix to develop](jenkins_deploy_to_hot-fix_to_develop.png "Hot-Fix to develop")

### Hot-Fix to master
![Hot-Fix to master](jenkins_deploy_to_hot-fix_to_master.png "Hot-Fix to master")

## Releases 

### Release to develop
![Release to develop](jenkins_deploy_to_release_to_develop.png "Release to develop")

### Release to master
![Release to master](jenkins_deploy_to_release_to_master.png "Release to master")


# Build from

## Tag
![Build from tag](jenkins_build_from_application_demo_build_from_tag.png "Build from tag")

## Branch
![Build from branch](jenkins_build_from_application_demo_build_from_branch.png "Build from branch")
