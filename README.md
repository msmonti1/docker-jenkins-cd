CI/CD GitLab+Jenkins+Docker Manual
==

- [Desired required knowledge](#Desired-required-knowledge)
- [Objectives](#Objectives)
- [Setting up the host environment](#Setting-up-the-host-environment)
- [Preparing GitLab](#Preparing-GitLab)
    - [Branch naming conventions](#Branch-naming-conventions)
- [Preparing Docker Registry](#Preparing-Docker-Registry)
- [Preparing Jenkins](#Preparing-Jenkins)
    - [Configuring Jenkins](#Configuring-Jenkins)
    - [Building our first jobs](#Building-our-first-jobs) 
- [Interacting with the repository](#Interacting-with-the-repository)
    - [Features](#Features)
    - [Releases](#Releases)
    - [Hot-Fixes](#Hot-Fixes)


## Desired required knowledge

Take a time to read the following links 

   - [GitFlow WorkFlow](https://nvie.com/posts/a-successful-git-branching-model/)
   - [Semantic Versioning](https://semver.org/)
   - [Jenkins](https://es.wikipedia.org/wiki/Jenkins)
   - [Docker](https://www.edureka.co/blog/docker-explained/#What%20is%20Docker?)
   
   
## Objectives

Implement [GitFlow WorkFlow](https://nvie.com/posts/a-successful-git-branching-model/) and create the needed jobs in Jenkins to automatize it using "dockerized" environments.
 
![GitFlow WorkFlow](https://nvie.com/img/git-model@2x.png) 

We will be able to:

 - Push to any branch --> test and deploy automatically.  
 - Release new version via Web (based on release branches)  
 - Release new hotfix and auto-increment the tag via Web (based on hot-fix branches)  
 - Deploy and test features before merging (based on release branches)  
 - Deploy any tag/branch.  
 
 - Keep our code versions as docker images.  
 
## Setting up the host environment.

We need to install Docker y Docker Compose following the official guide:

- Docker: 
    - https://docs.docker.com/install/linux/docker-ce/ubuntu/
    - https://docs.docker.com/install/linux/linux-postinstall/
    
- Docker Compose: 
    - https://docs.docker.com/compose/install/

> For Windows users follow this guide:
https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly

Check everything works fine:

```bash
docker info
```

## Preparing GitLab
###### Using docker-compose file

Before run the docker-compose we must to create a local volume for GitLab Database (PostgreSQL):

```bash
docker volume create --name gitlab-postgresql -d local
```

And modify the volumes labels of **1.GitLab/docker-compose.yml** (only for GitLab and Redis) with a local path where will be stored the data of the containers. 

> If you are running in Windows you must to enable the Shared Drives from the Docker Desktop app and add the path to docker-compose file with double slash, like in the example

> If you are running the code in Linux, is only needed to modify the volumes labels with a local path

Now, we can deploy GitLab executing the following command:

```bash
docker-compose -f 1.GitLab/docker-compose.yml up -d
```

This command will build the images required for GitLab and run the containers, it will take a couple of minutes to full start.  

We can access to the UI through *http://localhost:10080/*  

**User: root  
Password: 5iveL!fe**

After the first login we must change the password and create a **new project called app**.  
Once the project was created we can do our first commit & push:

```bash
./1.GitLab/createProjectsAndCommitToGitLab.sh
```

> This script will generate the first commit and tag of project, additionally will create the develop branch from the master branch

The next step is add the WebHook to the project in the following URL: http://localhost:10080/root/app/hooks  
To notify Jenkins about changes for push events the following URL need to be added
 
```text
http://<IpOfHostMachine>:8080/git/notifyCommit?url=http://<IpOfHostMachine>:10080/root/app.git
```

> Where IpOfHostMachine is your local IP (execute ipconfig or ifconfig)

In order to not lose your work when you shutdown the computer 
you must to run:

```bash
docker-compose -f 1.GitLab/docker-compose.yml stop
```

And the following to restore the container:

```bash
docker-compose -f 1.GitLab/docker-compose.yml start
```

### Branch naming conventions

 - develop
 - master
 
 
 - hot-fix/x.y.Z (Patch number must be incremented)
 - release/X.Y.Z
 - feature/*

> Useful link about semantic versioning https://semver.org/

## Preparing Docker Registry
###### Using Docker image

We will deploy a private Docker registry because we want to keep tagged in images our versions of code, not only in GitLab repository.  

To deploy the registry we must to run the following script:

```bash
./2.Registry/registry.sh
```

And add as insecure registry in our Docker [configuration](https://nickjanetakis.com/blog/docker-tip-50-running-an-insecure-docker-registry). 

## Preparing Jenkins
###### Using Dockerfile

Now we can deploy our Jenkins to start testing/deploying automatically.

Our Jenkins Dockerfile already include a sample job that must be modified and some needed plugins.

To start the Jenkins container the next script need to be executed:

```bash
cd 3.Jenkins+Docker
./buildAndDeploy.sh
```

This script will build the Jenkins image based in the Dockerfile and start it.  
We have configured that will install Docker to be able to build images and deploy it but will not a Docker server running, the Docker daemon is mapped to our host.

### Configuring Jenkins

#### Organizing the dashboard

We will create three views in order to group the jobs by they behavior:

  - Gitflow:
  
     - develop_branch
     - master_branch
     - feature_branches
     - release_branches
     - hot-fixes_branches
     
  - Merge_to
  
     - hot-fix_to_develop
     - hot-fix_to_master
     - release_to_develop
     - release_to_master

  - Build_from
  
     - application_demo_build_from_branch
     - application_demo_build_from_tag
     
> On the left side of Jenkins UI is the button to create the new view.

#### Adding environment variables

  - Manage Jenkins > Configure System > Global properties
  - Click on 'Environment variables'
  - Add:
    - CURRENT_IP --> IpOfHostMachine
    - GITLAB_PORT --> 10080

> Where IpOfHostMachine is your local IP (execute ipconfig or ifconfig)

#### Adding Gitlab credentials

  - [Credentials > System > Global credentials](http://localhost:8080/credentials/store/system/domain/_/)
  - Add Credentials
  - Set your Gitlab Username and password (root/12345678)
   
> When the credentials are created we must to **modify all the jobs** to set the Gitlab credentials.  
Click on: Name Job > Configure > Source Code Management  
Select the user added previously from the select box
 
#### Setting up Google Chat Notifications

> This step is optionally.

The jobs are configured to send notifications through a Google Chat group, to keep this feature up you need to modify 
all the URLs on the Google Chat Notification step, in all jobs.  
Follow this [link](https://github.com/jenkinsci/google-chat-notification-plugin) to add notifications on your Google Chat Group.  

### Building our first jobs

In this guide we will create jobs for this scenarios:  
  
  - Push to any branch in Gitflow Model (build & test).
  - Deploy and test a release branch (merge to master and develop). Parameterised.  
  - Deploy and test a hotfix branch updating the version patch number (merge to master and develop). Parameterised.
  - Deploy and test any branch. Parameterised.

> Currently the jobs are already implemented, take a minute to see them, they are divided by groups.
The grouped jobs are almost identical, just change branch names.

#### GitFlow jobs   

If we make a push to any of this branches, Jenkins will trigger the associated job.  

The following jobs are configured to:

  - Pull the last commit of the branch, run the unit test in the Dockerfile, build the Docker image and push it to a Docker Registry.    

[Develop branch](images/README.md#develop-branch)  
[Master branch](images/README.md#master-branch)  
[Feature branches](images/README.md#feature-branches)  
[Release branches](images/README.md#release-branches)  
[Hot-fixes branches](images/README.md#hot-fixes-branches)

#### Merge_to jobs

These jobs are activated manually and in a parameterized way.  
We can choose the branch to merge from a select box.

> The merge_to jobs for features branches will not be implement, it is better manually merge and review the code 
(Pull/Merge Request), perhaps more than one feature want to be merge to the next release and can exist merge conflicts.   
Once develop branch is ready we can prepare our release branch.

These jobs will look for any branch like **hot-fix/***  
[Hot-Fix to develop](images/README.md#hot-fix-to-develop)  
[Hot-Fix to master](images/README.md#hot-fix-to-master)

These jobs will look for any branch like **release/***  
[Release to develop](images/README.md#release-to-develop)  
[Release to master](images/README.md#release-to-master)

#### Build_from jobs

[Build from tag](images/README.md#tag)  
[Build from branch](images/README.md#branch)

## Interacting with the repository

### Features

At this point we are able to start using the GitFlow model.

Our repository currently contains:

  - develop branch
  - master branch (0.1.0 tagged)
  
The first step, create a feature:

```bash
cd ~/Development/app
git checkout -b feature/new-page develop
``` 

Now, we can modify the sample page (simulate a feature).
Change the text on *DEFAULT_STRING* variable on *app.py*, for example:

```python
DEFAULT_STRING = "Hello Web App running on Linux! Awesome feature"
``` 

Commit and push your changes:

```bash
git commit -am "New feature added"
git push origin feature/new-page:feature/new-page
``` 

>At this time we can access to the Jenkins user interface, we will see that all jobs have been activated in the Gitflow view, and some have failed.
This is because we push the repository and Jenkins will try to build all the branches, this happens only on the first push to the repository.

Okey, our feature is ready!!  
Is time to deploy a new release.

Once the feature have been tested we can proceed to open a pull/merge request.
  
On Gitlab project page navigate to [Merge request](http://localhost:10080/root/app/merge_requests) and open a new one 
choosing as source branch **feature/new-page** and **develop** as target branch, press "Compare branches", on this screen 
you can inform the name of the request and a description, also can assign the MR to someone or add some labels.
  
Press Submit.

In this case we do not have changes in develop branch not included on the feature branch, so we can press the button (and select *Remove source branch*) *Accept merge request*
and the code will automatically merged.

### Releases

Once the features are ready and successfully merged to the develop branch we can prepare our release.

First of all, pull remote changes:

```bash
git fetch
git checkout develop
git pull origin
``` 

And create the release branch from the develop

```bash
git checkout -b release/1.1.0 develop
```

Simulate a version upgrading modifying *DEFAULT_STRING* variable on *app.py*:

```python
DEFAULT_STRING = "Hello Web App running on Linux! Awesome feature. Version 1.1.0"
``` 

```bash
git commit -am "Version upgrade"
``` 

And modify again the variable

```python
DEFAULT_STRING = "Awesome feature. Version 1.1.0"
``` 

Commit and push your changes:

```bash
git commit -am "Release done"
git push origin release/1.1.0:release/1.1.0
``` 

Now go to the Jenkins UI, the push command triggered *release_branches* job.  
Once the job finish we can navigate to *Merge_to* view to merge the release to the master branch.

Click on **release_to_master > Build with Parameters** and choose our release branch from the select box.  
This job will merge the release branch to the master, later will run the **release_to_develop** job and **master_branch**, 
when  **release_to_develop** finish will run **develop_branch**.

> release_to_master job will push the second part of the branch as a new tag (*release/**1.1.0*** --> **1.1.0**)

If everything worked fine we can go to Gitlab to delete the branch or run the following command:

```bash
git push origin --delete release/1.1.0
```

> Is possible automate this merges thanks to the GitFlow model, on master branch only release branches can be merged 
(they never will have conflicts) and when a release is being preparing to be submitted the feature branches has to wait 
to be merged on develop branch, when the release is merged to master and develop branches we can merge the features.

### Hot-Fixes

Imagine that our last release has a bug but have been merged on master branch and deployed, we need to create a hot-fix branch, **the aim of hot-fix branches is just fix bugs on master branch**.

First of all, pull remote changes:

```bash
git fetch
git checkout master
git pull origin
``` 

Once we are aligned with the remote repository we can create our branch:

```bash
git checkout -b hot-fix/1.1.1 master
``` 

Simulate a bug fix modifying *DEFAULT_STRING* variable on *app.py*:

```python
DEFAULT_STRING = "Awesome fixed feature. Version 1.1.1"
``` 

Commit and push your changes:

```bash
git commit -am "Fixed bugs"
git push origin hot-fix/1.1.1:hot-fix/1.1.1
``` 

Now go to the Jenkins UI, the push command triggered *hot-fixes_branches* job.  
Once the job finish we can navigate to *Merge_to* view to merge the hot-fix to the master branch.

Click on **hot-fix_to_master > Build with Parameters** and choose our hot-fix branch from the select box.  
This job will merge the hot-fix branch to the master, later will run the **hot-fix_to_develop** job and **master_branch**, 
when  **hot-fix_to_develop** finish will run **develop_branch**.

> The job hot-fix_to_develop can fail if the develop branch contains changes that are not present on the last successful
 release, in this case the best approach to merge the hot-fix to the develop branch is by a Merge/Pull Request, manually reviewed

> hot-fix_to_master job will push a new tag upgrading the patch number of the last tag(*release/**1.1.0*** --> **1.1.1**)